#!/usr/bin/env python

import requests
import json

from authstuff import authapikey

# ## Configuration

# In[ ]:


#auth = ('your username','your password')
auth = None

### in authstuff.py, just have one line:
###  authapikey = 'xxxxxxxx-xxxx-xxxx-xxxxxxxxxxxx'
### where the magic string as shown by
###   clowder server -> profile pulldown at far upper right -> "API Keys" tab


clowderbase = "https://datavizhub.clowderframework.org"

authheaders = { 'X-API-Key':authapikey } 
headers = {'X-API-Key':authapikey, 'Content-type': 'application/json', 'accept': 'application/json'}



def list_collections():
    r = requests.get(clowderbase + '/api/collections', headers=headers)
    print(r.status_code)
    print(r.text)
    return json.loads(r.text)

def list_datasets():
    r = requests.get(clowderbase + '/api/datasets', headers=headers)
    print(r.status_code)
    print(r.text)
    return json.loads(r.text)


print("List collections")
cns = list_collections()

collection = cns[0]["id"]
print("collection", collection)


# ### create dataset


def create_dataset(name, description, access, space, collection):
    '''
     params: name, description, access: PUBLIC vs PRIVATE, 
         space: a list of string can be empty,
         collection: a list of string, can be empty
    '''

    payload = json.dumps({'name':name, 
                          'description':description,
                          'access':access,
                          'space':space,
                          'collection':collection}) 

    r = requests.post('https://clowderframework.org/clowder/api/datasets/createempty',
                     data=payload,
                     headers=headers,
                     auth=auth)
    print(r.status_code)
    print(r.text)


# In[ ]:


###create_dataset(name="new dataset", description="...", access="PRIVATE", 
###               space=['5a7c84a04f0cb43c8e37e206','5a3bef1c4f0cc6f36475b122'],
###              collection=['5a7c811b4f0cb43c8e37e1df'])


# ### edit the description for the dataset

# In[ ]:


def edit_dataset_description(dataset_id, description):

    payload = json.dumps({'description':description})
    
    r = requests.put('https://clowderframework.org/clowder/api/datasets/'
                     + dataset_id +'/description',
                     data=payload,
                     headers=headers,
                     auth=auth)
    print(r.status_code)
    print(r.text)


# In[ ]:


### edit_dataset_description('5aac36954f0cc56d89e141b8', 'new description')


# ### edit the dataset name

# In[ ]:


def edit_dataset_name(dataset_id, name):

    payload = json.dumps({'name': name})
    r = requests.put('https://clowderframework.org/clowder/api/datasets/' + dataset_id +'/title',
                     data=payload,
                     headers=headers,
                     auth=auth)
    print(r.status_code)
    print(r.text)


# In[ ]:


### edit_dataset_name('5aac36954f0cc56d89e141b8','new name')


# ### add tags to the dataset

# In[ ]:


def add_tags_to_dataset(dataset_id, tags):
    
    payload = json.dumps({'tags':tags})
    r = requests.post('https://clowderframework.org/clowder/api/datasets/' + dataset_id +'/tags',
                     data=payload,
                     headers=headers,
                     auth=auth)
    print(r.status_code)
    print(r.text)


# In[ ]:


### add_tags_to_dataset('5aac36954f0cc56d89e141b8',['tag1','tag2','tag3'])


# ### add user metadata to dataset

# In[ ]:


def add_metadata_to_dataset(dataset_id, metadata):
    '''
        metadata can be any key-value pair
        default metadat: Audience, CSDMS Standard Name, Date and Time, Funding Institution, 
            GeoJSON, Grant Number, ODM2 Variable Name, Primary/Initial Publication, Principal Investigator(s),
            References, Related Publications, SAS Spatial Geocode, SAS Variable Name, Time Periods, Unit
    '''
    
    payload = json.dumps(metadata)
    r = requests.post('https://clowderframework.org/clowder/api/datasets/' + dataset_id +'/metadata',
                     data=payload,
                     headers=headers,
                     auth=auth)
    print(r.status_code)
    print(r.text)


# In[ ]:


### add_metadata_to_dataset('5aac36954f0cc56d89e141b8', {'time':'now','author':'chen','email':'cwang138@illinois.edu'})


# ### delete dataset (careful!)

# In[ ]:


def delete_dtaset(dataset_id):
    r = requests.delete('https://clowderframework.org/clowder/api/datasets/'+ dataset_id,
                        headers=authheaders,
                        auth=auth)
    print(r.status_code)
    print(r.text)


# In[ ]:


### delete_dtaset('5aabd0354f0cc56d89e13d39')


# # Files

# ### upload files to that dataset (through url)

# In[ ]:


def upload_files_to_dataset(dataset_id, url):
    payload = json.dumps({'url':url})
    r = requests.post('https://clowderframework.org/clowder/api/datasets/' + dataset_id + '/urls',
                     data=payload,
                     headers=headers,
                     auth=auth)
    print(r.status_code)
    print(r.text)


# In[ ]:


### upload_files_to_dataset(dataset_id = '5a8f37744f0cfe889c135099',
###                         url = 'http://illinois.edu/assets/img/branding/wordmark_vertical.png')


# ### add tag to a file 

# In[ ]:


def add_tags_to_file(file_id, tags):
    '''
    tags has to be a list
    '''
   
    payload = json.dumps({'tags':tags})
    headers = {'Content-type': 'application/json', 'accept': 'application/json'}
    r = requests.post('https://clowderframework.org/clowder/api/files/' + file_id +'/tags',
                     data=payload,
                     headers=headers,
                     auth=auth)
    print(r.status_code)
    print(r.text)


# In[ ]:


### add_tags_to_file('5aac3abc4f0cc56d89e141ea',['tag1','tag2','tag3'])


# ### add description to a file

# In[ ]:


def add_description_to_file(file_id, description):

    payload = json.dumps({'description':description})
    r = requests.put('https://clowderframework.org/clowder/api/files/' 
                     + file_id +'/updateDescription',
                     data=payload,
                     headers=headers,
                     auth=auth)
    print(r.status_code)
    print(r.text)


# In[ ]:


### add_description_to_file('5aac3abc4f0cc56d89e141ea','lalala')


# ### add metadata to a file

# In[ ]:


def add_metadata_to_file(file_id, metadata):
    '''
     metadata can be any key-value pair.
     a few fields that defaulted by clowder: Audience, CSDMS Standard Name, Date and Time, Funding Institution, 
        GeoJSON, Grant Number, ODM2 Variable Name, Primary/Initial Publication, Principal Investigator(s),
        References, Related Publications, SAS Spatial Geocode, SAS Variable Name, Time Periods, Unit
    '''
    
    payload = json.dumps(metadata)
    headers = {'Content-type': 'application/json', 'accept': 'application/json'}
    r = requests.post('https://clowderframework.org/clowder/api/files/' + file_id +'/metadata',
                     data=payload,
                     headers=headers,
                     auth=auth)
    print(r.status_code)
    print(r.text)


# In[ ]:


### add_metadata_to_file('5aac3abc4f0cc56d89e141ea', {'Audience':'chen wang','lalal':'lalalala'})


# ### delete files (careful!)

# In[ ]:


def delete_file(file_id):
    
    r = requests.delete('https://clowderframework.org/clowder/api/files/'+ file_id, headers=authheaders, auth=auth)
    print(r.status_code)
    print(r.text)


# In[ ]:


### delete_file('5aac3abc4f0cc56d89e141ea')

