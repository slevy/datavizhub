
from django.urls import path

from . import views

urlpatterns = [
    path("requestor/list/", views.requestor_list),
    path("requestor/info/<str:requestor>", views.requestor_info),

    path("visitem/list/", views.visitem_list),
    path("visitem/lookup/<str:pattern>", views.visitem_lookup),
    path("visitem/byid/<int:id>", views.visitem_info),

    path("order/list/", views.order_list),  # with auth.
    path("order/lookup/<str:pattern>", views.order_lookup),
    path("order/byid/<int:id>", views.order_info),
    path("order/additem/<int:visitemid>", views.order_additem),
    path("order/" XXX

    path("
