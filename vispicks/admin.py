
from django.contrib import admin
from . import models

admin.site.register( models.Requestor )
admin.site.register( models.VisItem )
admin.site.register( models.Order )
admin.site.register( models.OrderedItem )
