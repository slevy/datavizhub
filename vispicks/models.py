#! /usr/bin/env python3

from django.db import models
import os

class VisItem(models.Model):
    """VisItem -- a choice from the collection of available videos, images, etc., including a URL in clowder.
    Might there be various domains that VisItems might come from, as in AVL, CITL, some-other-collection?
    Maybe VisItem needs a "domain" ForeignKey too.
    And would we want some licensing conditions, e.g. "educational use only", "ask Disney", "Creative Commons such-and-such", "free up to HD resolution", ... """

    url = models.URLField()
    title = models.CharField(max_length=80, blank=True)
    description = models.TextField(blank=True)
    # licensing_conditions?

    def __str__(self):
        return f"VisItem#{self.id} {self.title} ...{os.path.basename(str(self.url))}"

class Order(models.Model):
    """An order - a Requestor and some OrderedItems, which are tied to VisItems.
    Some items might require special licensing conditions, etc., so the purpose of use may matter.
    licenseterm should mean perpetual if blank."""

    requestor = models.ForeignKey(Person, on_delete=models.CASCADE)
    visitem = models.ManyToManyField(VisItem, through="OrderedItem")
    purpose = models.TextField(blank=True)
    licenseterm = models.CharField(blank=True)

    def __str__(self):
        return f"Order#{self.id} from {self.requestor} of {self.visitem}"

class OrderedItem(models.Model):
    """Connects VisItems and Orders.
    And if it was requested in a particular format ("HD", "4K", "stereo 4K", "dome 4K", etc.) that's recorded here too.
    If we distribute via a custom URL for that OrderedItem, record that here too.
    """

    visitem = models.ForeignKey(VisItem, on_delete=models.CASCADE)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    itemformat = models.CharField(max_length=42, blank=True)
    orderstatus = models.TextChoices('new',['new','submitted','awaitinginfo','approved','filled'])
    distributeurl = models.URLField()

    def __str__(self):
        return f"[item {self.visitem} order# {self.order.id} ({self.format})]"
    

class Requestor(models.Model):

    humanname = models.CharField(max_length=80)
    email = models.CharField(max_length=80)
    phone = models.CharField(max_length=20)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)

    def __str__(self):
        return f"From {self.humanname} <{self.email}>"
